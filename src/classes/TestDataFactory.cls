@isTest
public class TestDataFactory {
    private static Integer Num = 100;
    private static Map<String, String> recordTypeMap = RecordTypeSelector.getRecordTypeMap();



    /**
    * Create a list of Accounts
    * @param:
    * @return: Account records list
    *
    */
    public static List<Account> createAccounts(){
        List<Account> accList = new List<Account>();
        for(Integer i=0; i<Num; i++){
            Account acc = new Account();
            acc.name = 'TestAcc'+ i; 
            accList.add(acc);
        }
        return accList;
    }

    /**
    * Create a list of contacts whose record type is Independent_Candidate
    * @param: Accounts list
    * @return: contacts object list
    *
    */
    public static List<Contact> createCandidates(){
        List<Contact> canList = new List<Contact>();
        for(Integer i=0; i<Num; i++){
            Contact can = new Contact();
            can.recordTypeId = recordTypeMap.get('Independent_Candidate');
            can.lastName = 'TestCan' + i;
            canList.add(can);
        }
        return canList;
    }

    /**
    * Create a list of resume and files
    * @param: accounts list, Contacts list
    * @return: Resume and files object list
    *
    */
    public static List<PeopleCloud1__Web_Document__c> createWebDocList(List<Account> accList, List<Contact> canList){
        List<PeopleCloud1__Web_Document__c> webDocList = new List<PeopleCloud1__Web_Document__c>();
        for(Integer i=0; i<Num; i++){
            PeopleCloud1__Web_Document__c webDoc = new PeopleCloud1__Web_Document__c();
            webDoc.name = 'TestWebDoc' + i;
            webDoc.PeopleCloud1__Account__c = accList[(Integer) i/10].Id;
            webDoc.PeopleCloud1__Document_Related_To__c = canList[(Integer) i/10].Id;
            webDocList.add(webDoc);
        }
        return webDocList;
    }
}