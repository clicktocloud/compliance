/**
 * When test is running, a mock up field set is created in ContactComplianceExt class
 * a: document type has field set with expire date
 * b: document type has field set without expire date
 * c: document type does not have field set
 *
 * @author: Tony & Lina
 * @created:
 * @last modified: 10/03/2016
**/

@isTest
public class ContactComplianceExtTest {

    public static testmethod void testNewDoc(){
        ContactComplianceExt.Doc d = new ContactComplianceExt.Doc();
        d.status = 'F';
        d.name = 'a';

        system.assertEquals(d.name, 'a');
        system.assertEquals(d.status, 'F');
        system.assertEquals(d.color, 'btn-success');
    }

    // type a do not has a documents, return 'btn-danger'
    public static testMethod void testNewComplianceMissingFile() {
        Contact c = new Contact();
        c.lastname = 'testContact';
        insert c;

        ContactComplianceExt.Compliance comp = new ContactComplianceExt.Compliance('test', 'a;b;c', c.id);

        system.assertEquals(comp.documentList.size(), 3);
        system.assertEquals(comp.documentList.get(0).color, 'btn-danger');
        system.assertEquals(comp.documentList.get(1).color, 'btn-danger');
        system.assertEquals(comp.documentList.get(2).color, 'btn-danger');
        system.assertEquals(3, comp.getProblemDocumentList().size());
        system.assertEquals(comp.color, 'btn-danger');
    }

    public static testMethod void testNewComplianceNearExpire() {
        Contact c = new Contact();
        c.lastname = 'testContact';
        insert c;

        PeopleCloud1__Web_Document__c webDoc = new PeopleCloud1__Web_Document__c();
        webDoc.PeopleCloud1__Document_Type__c = 'a';
        webDoc.Expiry_Date__c = Date.today().addDays(10);
        webDoc.PeopleCloud1__Document_Related_To__c = c.id;
        insert webDoc;

        ContactComplianceExt.Compliance comp = new ContactComplianceExt.Compliance('test', 'a', c.id);
        system.assertEquals(comp.documentList.size(), 1);
        system.assertEquals(comp.documentList.get(0).color, 'btn-warning');
    }

    public static testMethod void testNewComplianceNotExpire() {
        Contact c = new Contact();
        c.lastname = 'testContact';
        insert c;

        PeopleCloud1__Web_Document__c webDoc = new PeopleCloud1__Web_Document__c();
        webDoc.PeopleCloud1__Document_Type__c = 'a';
        webDoc.Expiry_Date__c = Date.today().addDays(40);
        webDoc.PeopleCloud1__Document_Related_To__c = c.id;
        insert webDoc;

        ContactComplianceExt.Compliance comp = new ContactComplianceExt.Compliance('test', 'a', c.id);
        system.assertEquals(comp.documentList.size(), 1);
        system.assertEquals(comp.documentList.get(0).color, 'btn-success');
    }


    /**
     * for type a: should return F
     * for type b: should return P
     * for type c: should return F
    **/
    public static testMethod void testExt(){
        //create contact
        Contact cont = new Contact(lastname ='bob');
        insert cont;

        PeopleCloud1__CTCPeopleSettingHelper__c helper = new PeopleCloud1__CTCPeopleSettingHelper__c();
        helper.name = 'test';
        helper.PeopleCloud1__Resume_And_Files_Document_Type__c = 'a;b;c';
        helper.recordtypeid = [SELECT id, name FROM Recordtype WHERE name = 'CTC Compliance Requirements'].get(0).id;
        insert helper;

        // create document type a, the lastest expire date is not nearly expired
        PeopleCloud1__Web_Document__c webDoc = new PeopleCloud1__Web_Document__c();
        webDoc.PeopleCloud1__Document_Type__c = 'a';
        webDoc.Expiry_Date__c = Date.today().addDays(40);
        webDoc.PeopleCloud1__Document_Related_To__c = cont.id;
        insert webDoc;

        PeopleCloud1__Web_Document__c webDoc2 = new PeopleCloud1__Web_Document__c();
        webDoc2.PeopleCloud1__Document_Type__c = 'a';
        webDoc2.Expiry_Date__c = Date.today().addDays(20);
        webDoc2.PeopleCloud1__Document_Related_To__c = cont.id;
        insert webDoc2;

        PeopleCloud1__Web_Document__c webDoc3 = new PeopleCloud1__Web_Document__c();
        webDoc3.PeopleCloud1__Document_Type__c = 'a';
        webDoc3.PeopleCloud1__Document_Related_To__c = cont.id;
        insert webDoc3;

        // create document type b, the latest expire date is nearly expired
        PeopleCloud1__Web_Document__c webDoc4 = new PeopleCloud1__Web_Document__c();
        webDoc4.PeopleCloud1__Document_Type__c = 'b';
        webDoc4.Expiry_Date__c = Date.today().addDays(20);
        webDoc4.PeopleCloud1__Document_Related_To__c = cont.id;
        insert webDoc4;

        // create docuemnt type c
        PeopleCloud1__Web_Document__c webDoc5 = new PeopleCloud1__Web_Document__c();
        webDoc5.PeopleCloud1__Document_Type__c = 'c';
        webDoc5.PeopleCloud1__Document_Related_To__c = cont.id;
        insert webDoc5;

        ApexPages.StandardController sc = new ApexPages.standardController(cont);
        ContactComplianceExt ccExt = new ContactComplianceExt(sc);
        ccExt.complianceName = 'test';
        ccExt.changeDocumentList();

        system.assertEquals(3, ccExt.displayDocList.size());
        system.assertEquals('F', ccExt.displayDocList[0].status);
        system.assertEquals('F', ccExt.displayDocList[1].status);
        system.assertEquals('F', ccExt.displayDocList[2].status);
        system.debug(ccExt.displayDocList);
    }

}