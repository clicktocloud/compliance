@isTest
private class CTCPeopleSettingHelperSelectorTest {
    private static void insertTestHelper(String recordTypeLabel){
        PeopleCloud1__CTCPeopleSettingHelper__c t = new PeopleCloud1__CTCPeopleSettingHelper__c();
        t.recordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'PeopleCloud1__CTCPeopleSettingHelper__c' AND name = :recordTypeLabel LIMIT 1].get(0).id;
        t.name = 'test';
        insert t;
    }

    static testMethod void TestSelectByRecordTypeLabel(){
        String label = 'CTC Compliance Requirements';
        insertTestHelper(label);

        CTCPeopleSettingHelperSelector selector = new CTCPeopleSettingHelperSelector();
        List<PeopleCloud1__CTCPeopleSettingHelper__c> res = selector.getCTCPeopleSettingHelperByRecordTypeLabel(label);

        system.debug(res);
        system.debug([SELECT id, name, recordType.name FROM PeopleCloud1__CTCPeopleSettingHelper__c]);

        system.assertEquals(1, res.size());

        system.debug('Total Number: ' + [SELECT id FROM PeopleCloud1__CTCPeopleSettingHelper__c].size());
        system.debug('Compliance Number: ' + [SELECT id FROM PeopleCloud1__CTCPeopleSettingHelper__c WHERE recordType.name = :label].size());
    }
}