/**
 * Modified by linaw on 19/04/2017.
 */

public with sharing class ContactComplianceExt {
    private static final String COMPLIANCE_LABEL = 'CTC Compliance Requirements';
    private static final String FULL_COMPLIANT = 'F';
    private static final String PART_COMPLIANT = 'P'; // Nearly expired, but file is there
    private static final String NOT_COMPLIANT = 'N';
    private static final Map<String, String> colorMap;
    static {
        colorMap = new Map<String, String>();
        colorMap.put(FULL_COMPLIANT,'btn-success');
        colorMap.put(PART_COMPLIANT,'btn-warning');
        colorMap.put(NOT_COMPLIANT,'btn-danger');
    }
    private Map<String, List<Compliance>> complianceSetMap = new Map<String, List<Compliance>>();
    private Map<String, Compliance> complianceMap = new Map<String, Compliance>();

    private String parentId { get; set; }
    public List<Compliance> complianceList { get; set; }
    public String complianceName { get; set; }
    public List<Doc> displayDocList { get; set; }

    /**
     * Constructor
     */
    public ContactComplianceExt(ApexPages.StandardController stdController) {
        this.parentId = stdController.getId();
        CTCPeopleSettingHelperSelector helperSelector = new CTCPeopleSettingHelperSelector();
        List<PeopleCloud1__CTCPeopleSettingHelper__c> settingHelperList = helperSelector.getCTCPeopleSettingHelperByRecordTypeLabel(COMPLIANCE_LABEL);
        //init compliance list
        complianceList = new List<Compliance>();

        //Set Compliance Map
        for(PeopleCloud1__CTCPeopleSettingHelper__c sh : settingHelperList){
            Compliance c = new Compliance(sh.name, sh.PeopleCloud1__Resume_And_Files_Document_Type__c, parentId);
            complianceMap.put(c.name, c);
            complianceList.add(c);
        }

        //init problem docs
        displayDocList = new List<Doc>();
    }

    public void changeDocumentList() {
        // displayDocList = complianceMap.get(complianceName).getProblemDocumentList();
        displayDocList = complianceMap.get(complianceName).documentList;
    }

    public class Doc {
        public String name { get; set; }
        public String status { get; set; }

        public String color {
            get {
                return colorMap.get(status);
            }
            set;
        }
    }

    public class Compliance {
        public String name { get; set; }
        public String status { get; set; }
        public List<String> requiredFileTypeList { get; set; }
        public List<Doc> documentList { get; set; }
        public List<PeopleCloud1__Web_Document__c> actualDocumentList { get; set; }

        public Compliance(String name, String fileNameMultipickListString, String candidateId){
            ResumeAndFileSelector docSelector = new ResumeAndFileSelector();
            this.name = name;
            this.requiredFileTypeList = GenerateRequiredFileList(fileNameMultipickListString);
            this.actualDocumentList = docSelector.getResumeAndFileByContactAndFileType(candidateId, this.requiredFileTypeList);
            this.documentList = getDocumentList(this.requiredFileTypeList, this.actualDocumentList);
            this.status = getComplianceStatusByDocs(this.documentList);
        }

        public String color {
            get {
                return colorMap.get(status);
            }
            set;
        }

        public List<Doc> getProblemDocumentList() {
            List<Doc> ret = new List<Doc>();

            for(Doc doc : documentList){
                if(!doc.status.equals(FULL_COMPLIANT)){
                    ret.add(doc);
                }
            }

            return ret;
        }

        public List<String> GenerateRequiredFileList(String fileNameMultipickListString){
            List<String> ret = new List<String>();
            String[] temp = fileNameMultipickListString.split(';');

            for(String s : temp){
                ret.add(s);
            }

            return ret;
        }

        public Map<String, List<String>> getFieldSetMap() {
            Map<String, List<String>> fieldSetMap = new Map<String, List<String>>();

            // if test is running, return a mock up filed set map
            if (Test.isRunningTest()) {
                fieldSetMap.put('a', new List<String>{'Expiry_Date__c'});
                fieldSetMap.put('b', new List<String>());
                return fieldSetMap;
            }

            Map<String, Schema.FieldSet> fsMap =Schema.SObjectType.PeopleCloud1__Web_Document__c.fieldSets.getMap();
            Set<String> keySet = fsMap.keySet();

            for (String key : keySet) {
                List<String> fieldPaths = new List<String>();
                for (Schema.FieldSetMember fm : fsMap.get(key).getFields()) {
                    fieldPaths.add(fm.getFieldPath());
                }
                fieldSetMap.put(fsMap.get(key).getDescription(), fieldPaths);
            }
            System.debug('FieldSetMap: ' + FieldSetMap);
            return fieldSetMap;
        }


        /**
         * Given the document type list(Compliance), and list of a candidate's all documents for those types,
         * based on the expire date of these documents, return a list of docs
         * @param: typeList
         * @param: docs
         * @return: ret a list of doc(document type name and status), the status is determined based on the expire date of that type of documents
        **/
        private List<Doc> getDocumentList(List<String> typeList, List<PeopleCloud1__Web_Document__c> docs) {
            List<Doc> ret = new List<Doc>();

            Map<String, List<String>> fieldSetMap = getFieldSetMap();
            Set<String> fieldSetType = getFieldSetMap().keySet();

            for(String s : typeList){
                // this type has expire date field
                if (fieldSetType.contains(s)) {
                    Set<String> fieldPath = new Set<String>();
                    fieldPath.addAll(fieldSetMap.get(s));
                    if (fieldPath.contains('Expiry_Date__c')) {
                        List<PeopleCloud1__Web_Document__c> temp = getDocumentByType(s, docs);
                        Doc d = new Doc();
                        d.name = s;
                        if(temp.size() > 0){
                            d.status = checkDocumentIsExpire(temp);
                        } else {
                            d.status = NOT_COMPLIANT;
                        }
                        ret.add(d);
                        continue;
                    }
                }
                // this type does not have field set or it's field set do not have expire date
                Doc d = new Doc();
                d.name = s;
                if (!getDocumentByType(s, docs).isEmpty()) {
                    d.status = FULL_COMPLIANT;
                } else {
                    d.status = NOT_COMPLIANT;
                }
                ret.add(d);
            }
            return ret;
        }

        //return a list of document having specific document type
        private List<PeopleCloud1__Web_Document__c> getDocumentByType(String docType, List<PeopleCloud1__Web_Document__c> docs) {
            List<PeopleCloud1__Web_Document__c> ret = new List<PeopleCloud1__Web_Document__c>();

            for(PeopleCloud1__Web_Document__c doc : docs){
                if(doc.PeopleCloud1__Document_Type__c.equals(docType)){
                    ret.add(doc);
                }
            }

            return ret;
        }

        //return true if the lastest expire date have not nearly expired
        //FULL_COMPLIANT for not nearly expired, PART_COMPLIANT for nearly expired, NOT_COMPLIANT for expired
        private String checkDocumentIsExpire(List<PeopleCloud1__Web_Document__c> docs){
            String status = NOT_COMPLIANT;

            Date latestExpireDate = Date.today();

            for (PeopleCloud1__Web_Document__c doc : docs) {
                if (doc.Expiry_Date__c != null && doc.Expiry_Date__c >= Date.today()) {
                    // if one of the document's expire date is not nearly expired
                    if (doc.Expiry_Date__c >= Date.today().addDays(30)) {
                        return FULL_COMPLIANT;
                    } else {
                        status = PART_COMPLIANT;
                    }
                }
            }
            return status;
        }

        private String getComplianceStatusByDocs(List<Doc> docs){
            boolean notCompliantFlag = false;
            boolean partCompliantFlag = false;

            for(Doc doc : docs){
                if(doc.status.equals(NOT_COMPLIANT)){
                    notCompliantFlag = true;
                }
                if(doc.status.equals(PART_COMPLIANT)){
                    partCompliantFlag = true;
                }
            }

            if(notCompliantFlag == true){
                return NOT_COMPLIANT;
            } else if (partCompliantFlag == true) {
                return PART_COMPLIANT;
            } else {
                return FULL_COMPLIANT;
            }
        }

    }
}