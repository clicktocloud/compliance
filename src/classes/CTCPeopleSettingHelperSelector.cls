public with sharing class CTCPeopleSettingHelperSelector {

    private String queryString = 'SELECT id, Name, RecordTypeId, RecordType.Name, '
            + 'PeopleCloud1__AP_Approver_Fields__c, PeopleCloud1__AP_Biller_Contact_Fields__c, '
            + 'PeopleCloud1__AP_Biller_Fields__c, PeopleCloud1__AP_Employee_Fields__c, '
            + 'PeopleCloud1__AP_Invoice_Item_Fields__c, PeopleCloud1__AP_Owner_Fields__c, '
            + 'PeopleCloud1__Company_Entity_ID__c, CTC_Compliance_Set__c, PeopleCloud1__CTCPeopleSetting__c, '
            + 'PeopleCloud1__isActive__c, PeopleCloud1__Rate_Card_ID__c, PeopleCloud1__Resume_And_Files_Document_Type__c, '
            + 'PeopleCloud1__Rule_Group_ID__c FROM PeopleCloud1__CTCPeopleSettingHelper__c ';

    public List<PeopleCloud1__CTCPeopleSettingHelper__c> getCTCPeopleSettingHelperByRecordTypeLabel(String LabelName){
        String query = queryString + 'WHERE RecordType.name = :LabelName';
        return database.query(query);
    }
}