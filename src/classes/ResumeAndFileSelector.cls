public with sharing class ResumeAndFileSelector {
    final static String queryString = 'SELECT id, Name, Expiry_Date__c, '
            + 'PeopleCloud1__Account__c, PeopleCloud1__Candidate_Management__c, '
            + 'PeopleCloud1__Default_Doc__c, PeopleCloud1__Document_Related_To__c, '
            + 'PeopleCloud1__Document_Type__c, PeopleCloud1__File_Size__c, '
            + 'PeopleCloud1__isDelete__c, PeopleCloud1__Link_to_File_Sec__c, PeopleCloud1__Link_to_File__c, '
            + 'PeopleCloud1__ObjectKey__c, PeopleCloud1__S3_Folder__c, '
            + 'PeopleCloud1__Status__c, PeopleCloud1__Type__c, PeopleCloud1__Vacancy__c FROM PeopleCloud1__Web_Document__c ';
    /**
    *Get resume and files list which are related to contact and in certain types.
    *@param: contactId, docType
    *@return: webDocList
    **/
    public List<PeopleCloud1__Web_Document__c> getResumeAndFileByContactAndFileType(String contactId, List<String> docType){
        String query = queryString + 'WHERE PeopleCloud1__Document_Related_To__c = :contactId AND PeopleCloud1__Document_Type__c IN :docType';
        return database.query(query);
    }

    /**
    *Get resume and files list which are related to account list need to be updated.
    *@param: accIdSet
    *@return: webDocList
    **/
    public static list<PeopleCloud1__Web_Document__c> getWebDocListRelatedToAcc(Set<String> accIdSet){
        list<PeopleCloud1__Web_Document__c> webDocList = new list<PeopleCloud1__Web_Document__c>();
        String docType = 'Terms Of Business';
        String query = queryString + ' WHERE PeopleCloud1__Account__c IN : accIdSet AND ' +
                'PeopleCloud1__Document_Type__c =: docType';
        webDocList = Database.query(query);
        return webDocList; 
    }

    /**
    *Get resume and files list which are related to contact list need to be updated.
    *@param: conIdSet
    *@return: webDocList
    **/
    public static list<PeopleCloud1__Web_Document__c> getWebDocListRelatedToCon(Set<String> conIdSet){
        list<PeopleCloud1__Web_Document__c> webDocList = new list<PeopleCloud1__Web_Document__c>();
        String query = queryString + ' WHERE PeopleCloud1__Document_Related_To__c IN : conIdSet';
        webDocList = Database.query(query);
        return webDocList;
    }
}