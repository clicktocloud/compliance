public with sharing class RecordTypeSelector {
    /*
    *Get record type Map
    *@param
    *@return
    **/
    public static Map<String, String> getRecordTypeMap(){
        Map<String, String> rtMap = new Map<String, String>();
        String query = 'SELECT Id, developerName FROM recordType';
        List<recordType> rtList = new List<recordType>();
        rtList = Database.query(query);
        for(recordType rt : rtList){
            rtMap.put(rt.developerName, rt.Id);
        }
        return rtMap;
    }

}