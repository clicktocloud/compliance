@isTest
private class ResumeAndFileSelectorTest {
    public static testmethod void selectorTest(){
        List<Account> accList = TestDataFactory.createAccounts();
        insert accList;
        List<Contact> canList = TestDataFactory.createCandidates();
        insert canList;
        List<PeopleCloud1__Web_Document__c> webDocList = TestDataFactory.createWebDocList(accList, canList);
        webDocList[0].PeopleCloud1__Document_Type__c =  'Terms of Business';
        webDocList[1].PeopleCloud1__Document_Type__c = 'WHS INDUCTION CERTIFICATE';
        webDocList[2].PeopleCloud1__Document_Type__c = 'WHS INDUCTION CERTIFICATE';
        webDocList[3].PeopleCloud1__Document_Type__c = 'WHS INDUCTION CERTIFICATE';
        insert webDocList;
        Set<String> accSet = new Set<String>{accList[0].Id, accList[1].Id, accList[2].Id, accList[3].Id};
        Set<String> conSet = new Set<String>{canList[0].Id, canList[1].Id, canList[2].Id, canList[3].Id};
        String contactId = canList[0].Id;
        List<String> docType = new List<String>{'Terms of Business', 'WHS INDUCTION CERTIFICATE' };
        List<PeopleCloud1__Web_Document__c> webDocList1 = new List<PeopleCloud1__Web_Document__c>();
        ResumeAndFileSelector selector =new ResumeAndFileSelector();
        webDocList1 = selector.getResumeAndFileByContactAndFileType(contactId, docType);
        system.assertEquals(webDocList1.size(), 4);
        webDocList1 = ResumeAndFileSelector.getWebDocListRelatedToAcc(accSet);
        system.assertEquals(webDocList1.size(), 1);
        webDocList1 = ResumeAndFileSelector.getWebDocListRelatedToCon(conSet);
        system.assertEquals(webDocList1.size(), 40);
    }
} 